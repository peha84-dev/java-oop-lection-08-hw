package lection.eight.hw;

import lection.eight.hw.enums.Gender;
import lection.eight.hw.exceptions.GroupOverflowException;

public class Main {
    public static void main(String[] args) {

        String groupName = "Journalism";
        Group group = new Group(groupName);

        Student student10 = new Student("Grut", "I'm", Gender.TALKING_TREE, 11, groupName);

        addStudentToGroup(group, new Student("Volodymyr", "Petrenko", Gender.MALE, 1, groupName));
        addStudentToGroup(group, new Student("Vsevolod", "Andriienko", Gender.MALE, 2, groupName));
        addStudentToGroup(group, new Student("Rostyslav", "Petruk", Gender.MALE, 3, groupName));
        addStudentToGroup(group, new Student("Liudmyla ", "Andriiuk", Gender.FEMALE, 4, groupName));
        addStudentToGroup(group, new Student("Bohdan", "Petrych", Gender.MALE, 5, groupName));
        addStudentToGroup(group, new Student("Vira", "Andriievych", Gender.FEMALE, 6, groupName));
        addStudentToGroup(group, new Student("Nadiia", "Petriv", Gender.FEMALE, 7, groupName));
        addStudentToGroup(group, new Student("Liubov", "Andriiv", Gender.FEMALE, 8, groupName));

        addStudentToGroup(group, new Student("Grut", "I'm", Gender.TALKING_TREE, 11, groupName));
        System.out.println(group.areThereEquivalentStudents());
        addStudentToGroup(group, new Student("Grut", "I'm", Gender.TALKING_TREE, 11, groupName));
        System.out.println(group.areThereEquivalentStudents());
    }

    private static void addStudentToGroup(Group group, Student student) {
        try {
            group.addStudent(student);
        } catch (GroupOverflowException e) {
            System.out.println("\u001B[31m" + e.getMessage() + "\u001B[0m");
        }
    }

    private static void printGroupInfo(Group group) {
        System.out.println("\u001B[34m" + "-------------------------------------------" + "\u001B[0m");
        System.out.print("\u001B[34m" + group + "\u001B[0m");
        System.out.println("\u001B[34m" + "-------------------------------------------" + "\u001B[0m");
    }
}