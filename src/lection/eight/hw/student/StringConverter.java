package lection.eight.hw.student;

import lection.eight.hw.Student;

public interface StringConverter {
    public String toStringRepresentation (Student student);
    public Student fromStringRepresentation (String str);
}
