package lection.eight.hw.student;

import lection.eight.hw.Group;
import lection.eight.hw.Student;
import lection.eight.hw.enums.Gender;
import lection.eight.hw.exceptions.GroupOverflowException;

import java.util.InputMismatchException;
import java.util.Scanner;

public class StudentBuilder {

    public static Student createStudent() {
        Scanner sc = new Scanner(System.in);

        System.out.println("Add new student: ");
        System.out.print("  Input name: ");
        String name = sc.nextLine();
        System.out.print("  Input last name: ");
        String lastName = sc.nextLine();
        System.out.print("  Input gender (male or female): ");
        String newGender = sc.nextLine().toUpperCase();
        Gender gender;
        try {
            gender = Gender.valueOf(newGender);
        } catch (IllegalArgumentException e) {
            gender = Gender.TALKING_TREE;
        }

        int id = 0;
        while (id <= 0) {
            try {
                System.out.print("  Input ID number: ");
                id = sc.nextInt();
            } catch (InputMismatchException e) {
                sc.next();
                System.err.println("    Wrong input format. Input integer number!");
            }
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return new Student(name, lastName, gender, id);
    }

    public static void addToGroup(Student student, Group group) throws GroupOverflowException {
        group.addStudent(student);
    }
}
